<?php
$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if(isset($_GET['product'])){

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apollo Plywood</title>
    <link rel="stylesheet" href="../css/uikit.min.css">

    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <header>
        <div class="uk-container uk-padding-small desktop-header">
            <div class="uk-grid">
                <div class="uk-width-1-5@m">
                    <div class="site-logo__container">
                        <img data-src="./images/logo.jpg" class="site-logo__image"
                            alt="" uk-img>
                    </div>
                </div>
                <div class="uk-width-4-5@m uk-flex uk-flex-right uk-flex-middle uk-flex-right">
                    <div class="uk-grid uk-grid-medium uk-width-auto">
                        <div class="header-achievment uk-flex">
                            <div class="header-achievment__icon"><img width="" height="" alt=""
                                    uk-img="./images/IGBC.png" uk-svg></div>

                        </div>
                        <div class="header-achievment uk-flex ">
                            <div class="header-achievment__icon"><img width="" height="" alt=""
                                    uk-img="./images/ISO 9001.png" uk-svg></div>

                        </div>

                        <div class="header-achievment uk-flex ">
                            <div class="header-achievment__icon"><img width="" height="65px" alt=""
                                    uk-img="./images/ISO 14001.png" uk-img></div>

                        </div>
                        <div class="header-buttons uk-flex uk-flex-middle" >
                            <button class="uk-button uk-button-default contact-us--button" style="max-height:40px;">Contact us</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <nav class="main-desktop-nav uk-navbar-container uk-navbar-transparent" uk-navbar>
            <div class="uk-navbar-center">
                <ul class="main-desktop-nav__ul uk-navbar-nav">
                    <li class="main-desktop-nav__link"><a href="/" class="main-desktop-nav__link ">home</a></li>
                    <li class="main-desktop-nav__li"><a href="../about.php" class="main-desktop-nav__link">about us</a></li>
                    <li class="main-desktop-nav__li"><a href="#" class="main-desktop-nav__link">plywood</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="../products/bpw-grade-plywood-platinum.php">BWP Grade Plywood (Platinum)</a></li>
                                <li><a href="../products/bwp-grade-plywood-plus.php">BWP Grade Plywood (Plus)</a></li>
                                <li><a href="../products/bwp-grade-plywood-club.php">BWP Grade Plywood (Club)</a></li>
                                <li><a href="../products/bwp-grade-plywood.php">BWR Grade Plywood</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="main-desktop-nav__li"><a href="#" class="main-desktop-nav__link">block board</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li class=""><a href="../products/bpw-grade-block-board-platinum.php">BWP Grade Block Board (Platinum)</a></li>
                                <li><a href="../products/bpw-grade-block-board-club-plus.php">BWP Grade Block Board (Club Plus)</a></li>
                                <li><a href="../products/mr-grade-block-board.php">MR Grade Block Board</a></li>

                            </ul>
                        </div>
                    </li>
                    <li class="main-desktop-nav__li"><a href="#" class="main-desktop-nav__link">flush door</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="../products/bpw-grade-flush-door-platinum.php">BWP Grade Flush Door (Platinum)</a></li>
                                <li><a href="../products/bpw-grade-flush-door-club-plus.php">BWP Grade Flush Door (Club Plus)</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="main-desktop-nav__li"><a href="../products/shuttering-plywood.php" class="main-desktop-nav__link">shuttering ply</a></li>
                    <li class="main-desktop-nav__li"><a href="../products/flexi-plywood.php" class="main-desktop-nav__link">flexi ply</a></li>
                    <li class="main-desktop-nav__li"><a href="../media.php" class="main-desktop-nav__link">media center</a></li>
                    <li class="main-desktop-nav__li"><a href="../clients.php" class="main-desktop-nav__link">clients</a></li>
                    <li class="main-desktop-nav__li"><a href="../contact.php" class="main-desktop-nav__link">contact us</a></li>
                </ul>
            </div>
        </nav>

        <div class="uk-container uk-padding-small mobile-header">
            <div class="uk-grid">
                <div class="uk-width-4-5">
                    <div class="site-logo__container">
                        <img data-src="./images/logo.jpg" class="site-logo__image"
                             alt="" uk-img>
                    </div>
                </div>
                <div class="uk-width-1-5 mobile-menu-open--container">
                    <a href="#" uk-icon="icon: menu; ratio: 3;" id="mobile-menu-open"></a>
                </div>

            </div>
        </div>
        </div>
        <nav class="uk-navbar-container uk-navbar-transparent mobile-navigation-container">
            <a href="#" uk-icon="icon: close; ratio: 3;" id="mobile-menu-close"></a>
            <div class="mobile-navigation-container__div">
                <ul class="">
                    <li class=""><a href="/">home</a></li>
                    <li class=""><a href="about.php">about us</a></li>
                    <li class=""><a href="#" >plywood</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="../products/bpw-grade-plywood-platinum.php">BWP Grade Plywood (Platinum)</a></li>
                                <li><a href="../products/bwp-grade-plywood-plus.php">BWP Grade Plywood (Plus)</a></li>
                                <li><a href="../products/bwp-grade-plywood-club.php">BWP Grade Plywood (Club)</a></li>
                                <li><a href="../products/bwp-grade-plywood.php">BWR Grade Plywood</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class=""><a href="#">block board</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li class=""><a href="../products/bpw-grade-block-board-platinum.php">BWP Grade Block Board (Platinum)</a></li>
                                <li><a href="../products/bpw-grade-block-board-club-plus.php">BWP Grade Block Board (Club Plus)</a></li>
                                <li><a href="../products/mr-grade-block-board.php">MR Grade Block Board</a></li>

                            </ul>
                        </div>
                    </li>
                    <li class=""><a href="#">flush door</a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="../products/bpw-grade-flush-door-platinum.php">BWP Grade Flush Door (Platinum)</a></li>
                                <li><a href="../products/bpw-grade-flush-door-club-plus.php">BWP Grade Flush Door (Club Plus)</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class=""><a href="../products/shuttering-plywood.php" >shuttering ply</a></li>
                    <li class=""><a href="../products/flexi-plywood.php" >flexi ply</a></li>
                    <li class=""><a href="../media.php" >media center</a></li>
                    <li class=""><a href="../clients.php" >clients</a></li>
                    <li class=""><a href="../contact.php" >contact us</a></li>
                </ul>
            </div>
        </nav>


    </header>