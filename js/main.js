

var formType = document.querySelectorAll("input[name='form-type']");

formType.forEach((ele, index)=>{
   ele.addEventListener('change', (event)=>{
       if(ele.value == 'enquire'){
            document.querySelector('#for-enquire').innerHTML = `<input class="uk-input" type="text" placeholder="Organization " required>`;
       }else{
           document.querySelector('#for-enquire').innerHTML = ``;
       }
   })
});

document.querySelector('#mobile-menu-close').addEventListener("click", (event)=>{
    document.querySelector(".mobile-navigation-container").style.display = "none";
});
document.querySelector('#mobile-menu-open').addEventListener("click", (event)=>{
    document.querySelector(".mobile-navigation-container").style.display = "block";
});

document.querySelector(".enquiry-tab__contact").addEventListener('mouseover', (event)=>{
    event.currentTarget.style.right ="0px";
});
document.querySelector(".enquiry-tab__contact").addEventListener('mouseleave', (event)=>{
    event.currentTarget.style.right ="-75px";
});

document.querySelector(".enquiry-tab__enquiry").addEventListener('mouseover', (event)=>{
    event.currentTarget.style.right ="0px";
});
document.querySelector(".enquiry-tab__enquiry").addEventListener('mouseleave', (event)=>{
    event.currentTarget.style.right ="-75px";
});