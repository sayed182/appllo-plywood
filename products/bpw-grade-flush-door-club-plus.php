
<?php include_once '../header.php' ?>
<section class="product bg-img">

    <div class="uk-container">
        <div class="uk-child-width-expand@s" uk-grid>
            <div class="uk-width-1-5@m">
                <div class="product-content__image">

                    <img src="../images/product-page/hero_img/clubplus.jpg" alt="">
                </div>
            </div>
            <div class="uk-width-4-5@m">
                <div class="product-content">
                    <h2 class="content-title">
                        BWP Grade Flush Door (Club Plus)
                    </h2>
                    <h3 class="content-subtitle">
                        [IS:2202(Part-1):1999]
                    </h3>
                    <div class="product-short-description">
                        <p>Our Club Plus Flush door is made from high class selected veneers,
                            well searched pine/hardwood and is bonded with phenol formaldehyde resin. This provides all the key
                            functions of a top quality door including security, protection from weather, privacy and status value
                            which comes with their good look.</p> <p><b>Application:-</b> Doors</p> <p><b>Thickness (mm) :-</b> 25, 30, 32, 35, 38</p> <p><b>Standard Size (Inches) :-</b> <br>Height : 78, 81, 84 <br> Width : 26, 27, 30, 32, 33, 36, 38</p>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
<!---->
<!--<section class="product-specification">-->
<!--    <div class="uk-container">-->
<!--        <div class="product-specification-container uk-text-center">-->
<!--            <h1>Technical Specification</h1>-->
<!--            <p></p>-->
<!--        </div>-->
<!---->
<!--        <div class="product-specification-logo">-->
<!--            <div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!---->
<!--        <button class="uk-button uk-button-default uk-text-center" type="button" uk-toggle="target: #modal-example">Show Specification</button>-->
<!--    </div>-->
<!--</section>-->

<section class="connected-brand-slider">
    <div class="uk-container">
        <h1>Our Brands</h1>
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay:true; autoplay-interval: 3000 ">
            <ul class="uk-slider-items uk-child-width-1-3@s uk-child-width-1-5@l">
                <li>
                    <img src="../images/product-page/brands/Club1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club2.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club3.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club4.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club5.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club6.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus2.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus3.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus4.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus5.jpg" alt="">
                </li>

            </ul>
<!--            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>-->
<!--            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>-->

        </div>

    </div>
</section>


<!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Product Specification</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
        </p>
    </div>
</div>



<?php include_once '../footer.php' ?>