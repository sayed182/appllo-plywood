
<?php include_once '../header.php' ?>
<section class="product bg-img">

    <div class="uk-container">
        <div class="uk-child-width-expand@s" uk-grid>
            <div class="uk-width-1-5@m">
                <div class="product-content__image">

                    <img src="../images/product-page/hero_img/clubplus.jpg" alt="">
                </div>
            </div>
            <div class="uk-width-4-5@m">
                <div class="product-content">
                    <h2 class="content-title">
                        BWP Grade Block Board (Club Plus)
                    </h2>
                    <h3 class="content-subtitle">[IS:1659:2004]</h3>
                    <div class="product-short-description">
                        <p> Our Club Plus BWP Grade Block Board is made of 100% selected, treated and
                            searched pine wood, solid core veneer. It is fabricated as a five ply construction - a central core of
                            wooden strips, two cross bands and two gurjan face veneers on each side of the core assembly. The grain
                            direction of glued core veneers is at right angles to the grain direction of face veneers in adjacent layers.
                            Other parameters such as hot-pressing, press temperature and pressure etc, are same as in plywood
                            processing.
                            Usage :- Ideal for cabinets, almirahs, doors, partitions, shelves,roofing,fencing etc
                            Standard Thickness (mm) :- 16, 19, 25
                            Standard Size (mtr) :- 2.44x1.22, 2.44x0.92, 2.14x1.22, 2.14x0.92, 1.84x1.22, 1.84x0.92</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="product-specification">
    <div class="uk-container">
        <div class="product-specification-container uk-text-center">
            <h1>Technical Specification</h1>
            <p></p>
        </div>

        <div class="product-specification-logo">
            <div >

                <li class="uk-card uk-card-default uk-card-body">
                    <img src="../images/product-page/specifications/IS1659.png" alt="">
                </li>

            </div>
        </div>


<!--        <button class="uk-button uk-button-default uk-text-center" type="button" uk-toggle="target: #modal-example">Show Specification</button>-->
    </div>
</section>


<section class="connected-brand-slider">
    <div class="uk-container">
        <h1>Our Brands</h1>
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay:true; autoplay-interval: 3000 ">
            <ul class="uk-slider-items uk-child-width-1-3@s uk-child-width-1-5@l">
                <li>
                    <img src="../images/product-page/brands/Club1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club2.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club3.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club4.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club5.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Club6.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus2.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus3.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus4.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Plus5.jpg" alt="">
                </li>

            </ul>
<!--            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>-->
<!--            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>-->

        </div>

    </div>
</section>



<!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Product Specification</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
        </p>
    </div>
</div>



<?php include_once '../footer.php' ?>