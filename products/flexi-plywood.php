
<?php include_once '../header.php' ?>
<section class="product bg-img">

    <div class="uk-container">
        <div class="uk-child-width-expand@s" uk-grid>
            <div class="uk-width-1-5@m">
                <div class="product-content__image">

                    <img src="../images/product-page/hero_img/flexi.jpg" alt="">
                </div>
            </div>
            <div class="uk-width-4-5@m">
                <div class="product-content">
                    <h2 class="content-title">
                        Flexi Plywood
                    </h2>
                    <h3 class="content-subtitle"></h3>
                    <div class="product-short-description">
                        <p>Flexi Plywood is a form of plywood but it is extremely flexible. It doesn’t show any resistance to bending or rolling and can be twisted or curved into any shape without cheeping, cracking, or peeling. Unlike regular plywood, the grains in flexi ply runs in the same direction giving the plywood the bendy property and unmatched flexibility – all without losing its structural integrity. It is prepared with hot pressing technique with thermosetting BWR Grade Adhesive.</p> <p><b>Usage :-</b> Essential where bowed or curved shapes are required. </p><p><b>Standard Thickness (mm)</b> :- 4, 6, 8 </p><p><b>Standard Size (mtr) :-</b> 2.44x1.22</p>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>

<!--<section class="product-specification">-->
<!--    <div class="uk-container">-->
<!--        <div class="product-specification-container uk-text-center">-->
<!--            <h1>Technical Specification</h1>-->
<!--            <p></p>-->
<!--        </div>-->
<!---->
<!--        <div class="product-specification-logo">-->
<!--            <div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!---->
<!--        <button class="uk-button uk-button-default uk-text-center" type="button" uk-toggle="target: #modal-example">Show Specification</button>-->
<!--    </div>-->
<!--</section>-->

<section class="connected-brand-slider">
    <div class="uk-container">
        <h1>Our Brands</h1>
        <div uk-slider>
            <ul class="uk-slider-items uk-child-width-1-3@s uk-child-width-1-5@l">
                <li>
                    <img src="../images/product-page/brands/Club1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply1.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply2.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply3.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply4.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply5.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply6.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply7.jpg" alt="">
                </li>
                <li>
                    <img src="../images/product-page/brands/Ply8.jpg" alt="">
                </li>

<!--                <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>-->
<!--                <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>-->
        </div>

    </div>
</section>


<!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Product Specification</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
        </p>
    </div>
</div>



<?php include_once '../footer.php' ?>