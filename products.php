<?php
$product = isset($_GET["product"])?$_GET["product"]:"";
$data_raw = file_get_contents('content/products.json');
$data_assoc = json_decode($data_raw, true);
if(!array_key_exists($product, $data_assoc)){

}else{

    $data = $data_assoc[$product];
}



?>
<?php include_once 'header.php' ?>
<?php if(array_key_exists($product, $data_assoc)){?>
    <section class="product bg-img">

        <div class="uk-container">
            <div class="uk-child-width-expand@s" uk-grid>
                <div class="uk-width-1-5@m">
                    <div class="product-content__image">
                    <?php if (strlen($data["hero_img"]) < 1) {?>
                        <img src="./images/red-shuttering-ply-500x500.jpg" alt="">
                    <?php }else{ ?>
                        <img src="./images/product-page/hero_img/<?php echo $data["hero_img"]?>" alt="">
                    <?php } ?>
                    </div>
                </div>
                <div class="uk-width-4-5@m">
                    <div class="product-content">
                        <h2 class="content-title">
                            <?php echo $data["title"] ?>
                        </h2>
                        <div class="product-short-description">
                           <?php echo $data["content"] ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="product-specification">
        <div class="uk-container">
            <div class="product-specification-container uk-text-center">
                <h1>Technical Specification</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut aliquid cupiditate odit dicta, blanditiis commodi. Nesciunt iste in dolorem reiciendis.</p>
            </div>

            <div class="product-specification-logo">
                <div  uk-grid>

                    <?php
                    foreach ($data["tech_spec_list"] as $list){
                        ?>
                        <li class="uk-card uk-card-default uk-card-body">
                            <img src="./images/product-page/001-strong.svg" alt="">
                            <span><?php echo $list["text"]?></span>
                        </li>
                    <?php } ?>

                </div>
            </div>

            
        <button class="uk-button uk-button-default uk-text-center" type="button" uk-toggle="target: #modal-example">Show Specification</button>
        </div>
    </section>

    <section class="connected-brand-slider">
        <div class="uk-container">
            <h1>Our Brands</h1>
            <div uk-slider>
                <ul class="uk-slider-items uk-child-width-1-3@s uk-child-width-1-4@">
                    <?php
                    foreach ($data["brands"] as $image_name){
                        ?>
                        <li>
                            <img src="./images/product-page/brands/<?php echo $image_name ?>.jpg" alt="">
                        </li>
                    <?php } ?>

                </ul>
            </div>

        </div>
    </section>


    <!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Product Specification</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
        </p>
    </div>
</div>
<?php } else{ echo "Not Found";}?>


<?php include_once 'footer.php' ?>