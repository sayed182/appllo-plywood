<footer class="footer">
        <div class="uk-container">
            
            <div class="uk-flex uk-flex-between uk-flex-center footer-logo-container" uk-grid>
                <div class="">
                    <h4>Quick Links</h4>
                    <ul class="uk-list">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Clients</a></li>
                        <li><a href="#">Media Center</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>

                <div class="">
                    
                    <img data-src="./images/Footer_Logo.jpg" data-height="60px" class="footer-logo" uk-img>
                    <a href="" uk-icon="icon: facebook"></a>
                    <a href="" uk-icon="icon: instagram"></a>
                    <a href="" uk-icon="icon: linkedin"></a>
                    
                </div>

                <div class="">
                    <h4>Contact Info:</h4>
                    <ul class="uk-list">
                        <li>
                            <a href="https://goo.gl/maps/hUAQ6vhALcpnKVTK9">
                                Apollo Plywood Industries<br>
                                Vill: Nasirhat, PO: Dhankoilhat<br>
                                PS: Kaliyaganj, Dist: Uttar Dinajpur<br>
                                West Bengal, Pincode: 733129
                            </a>
                        </li>

                        <li>
                            <a href="wa.me/+918101864509"> +91-8101864509</a>
                        </li>

                        <li>
                            <a href="mailto:info@apolloplywwod.com">info@apolloplywwod.com</a>
                        </li>
                    </ul>
                    
                </div>
                
            </div>

        </div>
    <p class="uk-text-center uk-margin-medium-top">Copyright &copy; 2020 Apollo Plywood Industries. Powered by <a target="_blank" rel="noreferrer noopener" href="http://www.webaddictz.in" style="font-size: 16px;">Webaddictz</a></p>
    </footer>
    
    <div class="enquiry-tab enquiry-tab__contact">
        <a href="tel:918101864509"><span uk-icon="icon: receiver;"></span>&nbsp;&nbsp;Call Us</a>
    </div>

    <div class="enquiry-tab enquiry-tab__enquiry">
        <a href="contact.php?form-type=enquire" ><span uk-icon="icon: mail;"></span>&nbsp;&nbsp;Enquire</a>
    </div>

    <script src="../js/uikit.min.js"></script>
    <script src="../js/uikit-icons.min.js"></script>
    <script src="/js/main.js"></script>
</body>

</html>