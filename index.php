<?php include_once 'header.php' ?>
    
    <div class="main-slider">
        <div uk-slideshow="ratio: 16:9; animation:push; autoplay:true;" >
            <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true" >
                <li>
                    <img src="./images/slide-1.jpg" alt="" uk-cover>
                </li>
                <li><img src="./images/slide-2.jpg" alt="" uk-cover></li>
            </ul>
            <div class="slideshow-nav-container">
                <a href="#" uk-slideshow-item="previous" uk-icon="icon: chevron-left; ratio: 3"
                    class="uk-icon-link"></a>
                <a href="#" uk-slideshow-item="next" uk-icon="icon: chevron-right;  ratio: 3" class="uk-icon-link"></a>
            </div>
        </div>
    </div>

    <section class="uk-section-muted feature">
        <div class="uk-container">
            <div class="uk-child-width-expand@s uk-grid-divided" uk-grid>
                <div class="feature__container">
                    <div class="feature__first-column">
                        <div class="feature__number feature__number--first">
                            <span>
                                14
                            </span>
                            <p>Years in<br>Industry</p>
                        </div>
                        <div class="feature__number feature__number--second">

                            <span># 1</span>
                            <p>Emerging <br> Supplier</p>
                        </div>
                    </div>
                    <div class="feature__second-column">
                        <div class="feature__number feature__number--third">
                            <span>100%</span>
                            <p>Vocal for<br>Local</p>
                        </div>
                        <div class="feature__number feature__number--fourth">
                            <span>99.99%</span>
                            <p>Viro<br>Kill</p>
                        </div>
                    </div>
                </div>
                <div class="feature__text">
                    <h1>Experienced <span>2006</span> </h1>
                    <p>
                        Our vision has always been distinct and a dedicated one. We are
                        committed to deliver best of products guaranteed against every kind of damage and
                        deterioration through natural agents like termites, borer and other harmful
                        elements.We aim at reaching every household in the country.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="product-list">
        <h1>Product Range</h1>
        <div class="uk-container">
            <div class="product-list__slider-container" uk-slider>
                <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-5@m uk-grid">
                    <li>
                        <div>
                            <img src="./images/svg/sofa-1.svg" alt=""  width="100"  uk-svg>
                            <p>Plywood</p>
                        </div>
                    </li>
                    <li>
                        <div>
                        <img src="./images/svg/bunk-bed.svg" alt=""  width="100" uk-svg>
                        <p>Blockboard</p>
                        </div>
                    </li>
                    <li>
                        <div>
                        <img src="./images/svg/wardrobe.svg" alt=""  width="100" uk-svg>
                        <p>Shuttering Ply</p>
                        </div>
                    </li>
                    <li>
                        <div>
                        <img src="./images/svg/door.svg" alt=""  width="100" uk-svg>
                        <p>Flush Door</p>
                        </div>
                    </li>
                    <!-- <li>
                        <img src="./images/svg/001-closet.svg" alt="" uk-svg>
                        <p>Membrane Door</p>
                    </li> -->
                    <li>
                        <div>
                        <img src="./images/svg/furniture.svg" alt="" height="100" width="100" uk-svg>
                        <p>Flexi Ply</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <div>
    </div>
    <section class="solutions">
        <div class="uk-container">
            <!-- <h1>Solutions for home and Office</h1> -->
            <div class="uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>

                <div>
                    <div class="solutions-heading-card">
                        <h3>Solutions for <span>Home</span> and <span>Office</span></h3>
                        <p>Some impressive works done with our range of products.</p>

                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle uk-transition-toggle uk-overflow-hidden"
                        tabindex="0" data-src="./images/showcase/5.jpg" uk-img>
                        <img data-src="./images/showcase/5.jpg" width="" height="" alt=""
                            uk-img>

                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle  uk-transition-toggle uk-overflow-hidden"
                        tabindex="0" data-src="./images/showcase/2.jpg" uk-img>
                        <img data-src="./images/showcase/2.jpg"
                            width="" height="" alt="" 
                             uk-img>


                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle  uk-transition-toggle uk-overflow-hidden"
                        tabindex="0" data-src="./images/showcase/4.jpg" uk-img>
                        <img data-src="./images/showcase/4.jpg"
                            width="" height="" alt=""
                            uk-img>


                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle  uk-transition-toggle uk-overflow-hidden"
                        tabindex="0" data-src="./images/showcase/1.jpg" uk-img><img data-src="./images/showcase/1.jpg"
                            width="" height="" alt="" 
                             uk-img>


                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle  uk-transition-toggle uk-overflow-hidden"
                        tabindex="0" data-src="./images/showcase/3.jpg" uk-img><img data-src="./images/showcase/3.jpg"
                            width="" height="" alt=""
                           uk-img>
                            

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial-slider">
        <div class="uk-container">
            <h1>Testimonials</h1>
            <div uk-slider>
                <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m uk-grid">
                    <li>
                        <div class="uk-card uk-card-default uk-card-body">
                            <p>Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <small>-Jone Doe</small>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-default uk-card-body">
                            <p>Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <small>-Jone Doe</small>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-default uk-card-body">
                            <p>Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <small>-Jone Doe</small>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-default uk-card-body">
                            <p>Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <small>-Jone Doe</small>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-default uk-card-body">
                            <p>Lorem ipsum sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <small>-Jone Doe</small>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

<?php include_once 'footer.php' ?>