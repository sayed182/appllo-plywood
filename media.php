<?php
$images =[];
$certificates =[];
$videos = [];
$images_dir = "images/gallery/images/";
$certificates_dir = "images/gallery/certificates/";
$videos_dir = "images/gallery/videos/";

$allowed_ext = ['jpeg', 'jpg', 'png'];
if (is_dir($images_dir)){
    if ($dh = opendir($images_dir)){
        while (($file = readdir($dh)) !== false){
            if(strlen($file) > 2){
                $ext = explode('.',$file)[count(explode('.',$file)) -1];
                if(in_array($ext,$allowed_ext)){
                    array_push($images, $file);
                }

            }
        }
        closedir($dh);
    }
}

if (is_dir($certificates_dir)){
    if ($dh = opendir($certificates_dir)){
        while (($file = readdir($dh)) !== false){
            if(strlen($file) > 2){
                array_push($certificates, $file);
            }
        }
        closedir($dh);
    }
}

if (is_dir($videos_dir)){
    if ($dh = opendir($videos_dir)){
        while (($file = readdir($dh)) !== false){
            if(strlen($file) > 2){
                array_push($videos, $file);
            }
        }
        closedir($dh);
    }
}

?>


<?php include_once 'header.php' ?>



    <section class="media-container">
        <h1 class="media-center-heading">
            Media Center
        </h1>
        <div class="uk-container">
            <div uk-filter="target: .js-filter">

                <ul class="media-nav uk-subnav uk-subnav-pill">
                    <li class="uk-active" uk-filter-control="[data-type='certificate']"><a href="#">Certification</a></li>
                    <li uk-filter-control="[data-type='picture']"><a href="#">Picture</a></li>
                    <li uk-filter-control="[data-type='video']"><a href="#">Video</a></li>
                </ul>
            
                <ul class="js-filter uk-child-width-1-2 uk-child-width-1-3@m uk-text-center" uk-grid>

                    <!--For Certificates -->
                    <?php if(count($certificates) > 0){
                        foreach($certificates as $image){
                            ?>
                            <li data-type="certificate">
                                <div class="uk-card uk-card-default uk-card-body"><img src="<?php echo $certificates_dir."/".$image; ?>" alt=""></div>
                            </li>
                            <?php
                        }}else{
                        echo '<li data-type="certificate">No Image here</li>';
                    }
                    ?>
                    <!-- End Certificates -->



<!--                    For Photoes -->
                    <?php if(count($images) > 0){
                        foreach($images as $image){
                    ?>
                    <li data-type="picture">
                        <div class="uk-card uk-card-default uk-card-body"><img src="<?php echo $images_dir."/".$image; ?>" alt=""></div>
                    </li>
                    <?php
                        }}else{
                        echo '<li data-type="picture">No Image here</li>';
                        }
                        ?>
<!--                    End Photoes-->
                    <li data-type="video">
                        <div class="uk-card uk-card-default uk-card-body"><img src="https://picsum.photos/400/300?random=2&grayscale" alt=""></div>
                    </li>

                </ul>
            
            </div>
        </div>
    </section>

<?php include_once 'footer.php' ?>