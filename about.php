<?php include_once 'header.php' ?>

    <section class="aboutus-main-section ">
        
        <div class="main-header">
            <h1>Who we are</h1>
        </div>
        <div  class="uk-container who-we-are" >
            <div class="uk-child-width-expand@s" uk-grid>
                <div class="who-we-are-content">
                    <p>Apollo Plywood Industries has been around for over a decade and a half.The factory
is located at Kaliyaganj in the district of North Dinajpur,the northern part of West
Bengal.Since our inception, the focus has always been on developing quality
products at an affordable price. Our expertise lies in manufacturing and making all
variants of plywood, block board & flush door. Our products are marine graded and
borer proof stated, Integrated with warp-free and anti-termite features.We employ
skilled,semi-skilled and unskilled personnel on our team to maintain high standards
of our products.Today, we are one of the leading industry torchbearers, supplying
calibre products to corporate houses, architects, designers and individuals.
</p> <p><b>Our Vision :</b> Our vision has always been distinct and a dedicated one. We are
committed to deliver best of products guaranteed against every kind of damage and
deterioration through natural agents like termites, borer and other harmful
elements.We aim at reaching every household in the country.</p>
<p><b>Our Mission :</b> Our mission is as simple as our working style. We aim at
manufacturing best quality products that’s high on innovation, providing exceptional
service to our customers who look upon us with desirable expectations.</p>
<p><b>Why us :</b> Our management is sincere enough to take care of the entire production
process and efficiently execute the orders of clients. For us, ‘Quality is not an act, it
is a habit’.</p>
                </div>
            </div>
        </div>
        

    </section>

    <section class="our-team">
        <div class="uk-container">
            <h2>Our Team</h2>
           
            <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                <div>
                    <div class="team-card-container uk-inline-clip uk-transition-toggle" tabindex="0">
                        <img src="./images/team/Sukumar%20Sarkar.jpg" alt="">
                            <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                            <p class="uk-text-center">Sukumar Sarkar</p>
                            </div>
                    </div>
                </div>
                <div>
                    <div class="team-card-container uk-inline-clip uk-transition-toggle" tabindex="0">
                        <img src="./images/team/Raj%20Kumar%20Jalan.jpg" alt="">
                        <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                        <p class="uk-text-center">Raj Kumar Jalan</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-card-container uk-inline-clip uk-transition-toggle" tabindex="0">
                        <img src="./images/team/Shubhadip%20Sarkar.jpg" alt="">
                        <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                        <p class="uk-text-center">Shubhadip Sarkar</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-card-container uk-inline-clip uk-transition-toggle" tabindex="0">
                        <img src="./images/team/Subhamoy%20Sarkar.jpg" alt="">
                        <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                        <p class="uk-text-center">Subhamoy Sarkar</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="team-card-container uk-inline-clip uk-transition-toggle" tabindex="0">
                        <img src="./images/team/Rajarshi%20Samanta.jpg" alt="">
                        <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                            <p class="uk-text-center">Raj Samanta</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-infrastructure ">
        <div class="uk-container">
        <h2>Our infrastructure</h2>
                       
                <div class="uk-child-width-expand@s" uk-grid>

                    <div class="our-infrastructure-content">
                        <p>Our unit has 2.5 lakhs sq.ft. area out of which 1.5 lakhs sq.ft. area
                            is covered and has all types of advanced plywood, block board and flush door
                            manufacturing machineries. We have an excellent in house stay for workers with
                            proper working environment and a dedicated R&D team who regularly figures out
                            new technologies to improve the standards of the product.We are an ISO 9001:2015
                            & ISO 14001:2015 business organisation and members of Indian Plywood Industries
                            Research & Training Institute(IPIRTI) and Indian Green Building Council(IGBC).
                            </p>
                    </div>
                </div>

            </div>  
    </section>

    <section class="our-process">
        <div class="uk-container">
        <h2>Process</h2>
                       
                <div class="uk-child-width-expand@s" uk-grid>
                    
                    <div class="our-process-content">
                        <p>Our products are produced through an automated manufacturing process
                        which ensures uniform quality on every parameters. State- of- the- art machines that 
                        ensure perfection at every step, numerically controlled and calibrated temperature,
                        pressure and duration, and automated material transfer helps maintain quality and
                        also conserve energy.
                            </p>
                        <img src="./images/about-images/Process%20Flow%20Chart.png" alt="" class="uk-align-center uk-display-block uk-width-1-2@m">
                    </div>
                </div>

            </div>  
    </section>
<?php include_once 'footer.php' ?>