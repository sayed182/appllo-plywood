<?php
$is_enquire = false;
if(isset($_GET["form-type"])){
    $is_enquire = $_GET["form-type"] == "enquire" ? true : false;
}

?>
<?php include_once 'header.php' ?>

    <section class="contact-us bg-img">
        <h1 class="">Contact Us</h1>
        <form>
            <fieldset class="uk-fieldset">
                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                    <label><input class="uk-checkbox form-type" type="radio" name="form-type" value="contact" <?php if(!$is_enquire) echo "checked"; ?>> General Enquiry</label>
                    <label><input class="uk-checkbox form-type" type="radio" name="form-type" value="enquire" <?php if($is_enquire) echo "checked"; ?> > Dealer Enquiry</label>
                </div>


                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Name">
                </div>
        
                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Email">
                </div>
        
                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Phone Number">
                </div>
                

                <div class="uk-margin" id="for-enquire">
                    <?php if($is_enquire){?>
                    <input class="uk-input" type="text" placeholder="Organization ">
                    <?php }?>
                </div>
        
             
        
                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="5" placeholder="Some important message to the owner"></textarea>
                </div>
        
            </fieldset>
            <button type="button" class="uk-button uk-button-default uk-width-1-1">submit</button>
        </form>
    </section>

 <?php include_once 'footer.php' ?>