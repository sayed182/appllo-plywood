<?php
$images =[];

$images_dir = "images/gallery/clients/";


$allowed_ext = ['jpeg', 'jpg', 'png'];
if (is_dir($images_dir)){
    if ($dh = opendir($images_dir)){
        while (($file = readdir($dh)) !== false){
            if(strlen($file) > 2){
                $ext = explode('.',$file)[count(explode('.',$file)) -1];
                if(in_array($ext,$allowed_ext)){
                    array_push($images, $file);
                }

            }
        }
        closedir($dh);
    }
}

?>


<?php include_once 'header.php' ?>



    <section class="media-container">
        <h1 class="media-center-heading">
            Clients
        </h1>
        <div class="uk-container">
            <div uk-filter="target: .js-filter">
            
                <ul class="js-filter uk-child-width-1-2 uk-child-width-1-3@m uk-text-center" uk-grid>




<!--                    For Photoes -->
                    <?php if(count($images) > 0){
                        foreach($images as $image){
                    ?>
                    <li data-type="picture">
                        <div class="uk-card uk-card-default uk-card-body"><img src="<?php echo $images_dir."/".$image; ?>" alt=""></div>
                    </li>
                    <?php
                        }}else{
                        echo '<li data-type="picture">No Image here</li>';
                        }
                        ?>
<!--                    End Photoes-->

                </ul>
            
            </div>
        </div>
    </section>

<?php include_once 'footer.php' ?>